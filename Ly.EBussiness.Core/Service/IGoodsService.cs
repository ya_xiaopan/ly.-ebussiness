﻿using Ly.EBussiness.Core.Dto;
using Volo.Abp.Application.Services;
using Volo.Abp.DependencyInjection;

namespace Ly.EBussiness.Core.Service;

public interface IGoodsService: ITransientDependency
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public Task PutGoodsAsync(PutGoodsDto input);

    /// <summary>
    /// 分页查询-上架商品
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public Task<PutGoodsPageOutput> QueryPutGoodsAsync(PutGoodsInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task PutDownGoodsAsync(int id);
}