﻿using Ly.EBussiness.Core.Dto.Order;
using Volo.Abp.DependencyInjection;

namespace Ly.EBussiness.Core.Service;

public interface IOrderService : ITransientDependency
{
    /// <summary>
    /// 查询自己的订单列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public Task<OrderListPageOutput> QueryOrdersInfoPageAsync(OrderListInput input);

    /// <summary>
    /// 查询自己的订单详情
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public Task<OrderDetailsDto> QueryOrderDetailAsync(OrderDetailsInput input);

    /// <summary>
    /// 下单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<bool> OrderAsync(OrderDto input);
}