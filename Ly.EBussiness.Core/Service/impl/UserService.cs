﻿using Ly.EBussiness.Core.Dto.User;
using Ly.EBussiness.Core.Manager;
using Ly.EBussiness.Core.Manager.Dto.LoginDto;
using Ly.EBussiness.EntityFrameworkCore.Auth;
using Ly.EBussiness.EntityFrameworkCore.Auth.User;

namespace Ly.EBussiness.Core.Service.impl;

public class UserService : IUserService
{
    private readonly LoginManager _loginManager;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="loginManager"></param>
    public UserService(LoginManager loginManager)
    {
        _loginManager = loginManager;
    }

    /// <summary>
    /// 用户登录-简易
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task<LoginResult> UserLoginAsync(UserLoginInput input)
    {
        var result = await _loginManager.LoginAsync(input);
        if (!result.Success) return result;
        var token = JwtHelp.getToken(result.UserInfo.UserId, result.UserInfo.UserName,Enum.GetName(result.UserInfo.Role) ?? Role.CustomUser);
        return result.FillToken(token);
    }
}