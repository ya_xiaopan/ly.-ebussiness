using Ly.EBussiness.Core.Dto.User;
using Ly.EBussiness.Core.Manager.Dto.LoginDto;
using Volo.Abp.DependencyInjection;

namespace Ly.EBussiness.Core.Service;

public interface IUserService : ITransientDependency
{
    // 简单得用户登录
    Task<LoginResult>UserLoginAsync(UserLoginInput input);
}