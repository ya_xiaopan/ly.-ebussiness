﻿using Ly.EBussiness.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Autofac;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace Ly.EBussiness.Core;

[DependsOn(typeof(AbpAutoMapperModule))]
[DependsOn(typeof(EntityFrameworkCoreModule))]
[DependsOn(typeof(AbpAutofacModule))]
public class CoreModule: AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddMaps<CoreModule>();
            options.AddMaps<EntityFrameworkCoreModule>();
        });
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        // SkipAutoServiceRegistration = true;
    }
}