﻿using Ly.EBussiness.Core.Dto.User;

namespace Ly.EBussiness.Core.Manager.Dto.LoginDto;

public class LoginResult
{
    public bool Success { get; set; }

    public UserInfoDto UserInfo { get; set; }
    
    public string Token { get; set; }
    public LoginResult LoginError()
    {
        Success = false;
        UserInfo = new UserInfoDto();
        return this;
    }
    
    public LoginResult LoginSuccess(UserInfoDto userInfo)
    {
        Success = true;
        UserInfo = userInfo;
        return this;
    }
    
    public LoginResult FillToken(string token)
    {
        Token = token;
        return this;
    }
}



