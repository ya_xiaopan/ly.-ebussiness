﻿using AutoMapper;
using Ly.EBussiness.Core.Dto;
using Ly.EBussiness.Core.Dto.Order;
using Ly.EBussiness.EntityFrameworkCore.Entity;

namespace Ly.EBussiness.Core;

public class CommonMapper:Profile
{
    public CommonMapper()
    {
        CreateMap<PutGoodsDto, PutGoods>().ReverseMap();
        CreateMap<ExpressInfoDto, ExpressInfo>().ReverseMap();
    }
}