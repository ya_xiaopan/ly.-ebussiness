﻿using Ly.EBussiness.EntityFrameworkCore.Entity;

namespace Ly.EBussiness.Core.Dto.Order;

/// <summary>
/// 下单
/// </summary>
public class OrderDto
{
    /// <summary>
    /// 商品详情
    /// </summary>
    public List<PutGoodForOrder> PutGoodForOrders { get; set; }

    /// <summary>
    /// 快递信息
    /// </summary>
    public ExpressInfoDto ExpressInfoDto { get; set; }

    
    /// <summary>
    /// 
    /// </summary>
    public void Check()
    {
        if (ExpressInfoDto.RecipientProvince.IsNullOrWhiteSpace() ||
            ExpressInfoDto.RecipientCity.IsNullOrWhiteSpace() ||
            ExpressInfoDto.AcctExpress.IsNullOrWhiteSpace() ||
            ExpressInfoDto.RecipientName.IsNullOrWhiteSpace() ||
            ExpressInfoDto.RecipientPhone.IsNullOrWhiteSpace())
        {
            throw new Exception("快递信息有误 请重新下单");
        }
    }
}

/// <summary>
/// 下单用
/// </summary>
public class PutGoodForOrder
{
    /// <summary>
    /// 上架id
    /// </summary>
    public int PutGoodId { get; set; }

    /// <summary>
    /// 份数
    /// </summary>
    public int Sum { get; set; }
}

/// <summary>
/// 下单用
/// </summary>
public class ExpressInfoDto
{
    /// <summary>
    /// 收件省份
    /// </summary>
    public string? RecipientProvince { get; set; }

    /// <summary>
    /// 收件城市
    /// </summary>
    public string? RecipientCity { get; set; }

    /// <summary>
    /// 详细地址
    /// </summary>
    public string? AcctExpress { get; set; }

    /// <summary>
    /// 收件人姓名
    /// </summary>
    public string? RecipientName { get; set; }

    /// <summary>
    /// 收件人电话
    /// </summary>
    public string? RecipientPhone { get; set; }

    /// <summary>
    /// 快递备注
    /// </summary>
    public string? ExpressComment { get; set; }
}