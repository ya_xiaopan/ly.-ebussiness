﻿using Ly.EBussiness.EntityFrameworkCore.Entity;
using Ly.EBussiness.EntityFrameworkCore.Entity.Enum;

namespace Ly.EBussiness.Core.Dto.Order;

/// <summary>
/// 订单详情
/// </summary>
public class OrderDetailsDto
{
    /// <summary> 订单号 </summary>
    public string OrderId { get; set; }
    
    /// <summary>  </summary>
    public List<PutGoodsDtoForOrder> PutGoodsDtoForOrder { get; set; }
    
    /// <summary> 总金额 </summary>
    public decimal TotalPrice { get; set; }
    

    public void Init(OrderInfo order, IList<OrderDetail> orderDetails, IList<PutGoods> putGoodsList)
    {
        OrderId = order.OrderId;
        TotalPrice = order.TotalAmount;
        var putGoodsDtoForOrders = new List<PutGoodsDtoForOrder>();
        foreach (var orderDetail in orderDetails)
        {
            var putGoodsDtoForOrder = new PutGoodsDtoForOrder();
            putGoodsDtoForOrder.TotalAmout = orderDetail.TotalPrice;
            putGoodsDtoForOrder.Sum = orderDetail.Sum;
            var firstOrDefault = putGoodsList.FirstOrDefault(x => x.Id == orderDetail.PutGoodsId) ?? new PutGoods();

            putGoodsDtoForOrder.GoodsType = firstOrDefault.GoodsType;
            putGoodsDtoForOrder.GoodsLevel = firstOrDefault.GoodsLevel;
            putGoodsDtoForOrder.GoodsUnit = firstOrDefault.GoodsUnit;
            putGoodsDtoForOrder.Price = firstOrDefault.OriginalPrice;
            
            putGoodsDtoForOrders.Add(putGoodsDtoForOrder);
        }
        PutGoodsDtoForOrder = putGoodsDtoForOrders;
    }
}



/// <summary>
/// 订单详情
/// </summary>
public class PutGoodsDtoForOrder
{
    /// <summary> 商品类型 </summary>
    public GoodsType GoodsType { get; set; }

    /// <summary> 商品大小 - level </summary>
    public GoodsLevel GoodsLevel { get; set; }

    /// <summary> 商品单位 </summary>
    public GoodsUnit GoodsUnit { get; set; }

    /// <summary> 单价 </summary>
    public decimal Price { get; set; }

    /// <summary> 数量</summary>
    public int Sum { get; set; }
    
    /// <summary> 单价 </summary>
    public decimal TotalAmout { get; set; }
    
}


/// <summary>
/// 订单详情
/// </summary>
public class OrderDetailsInput
{
    /// <summary> 订单号 </summary>
    public string OrderId { get; set; }
}