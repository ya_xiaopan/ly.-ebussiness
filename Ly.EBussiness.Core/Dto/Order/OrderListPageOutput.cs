﻿using Ly.EBussiness.Common.DapperUtils.Page;
using Ly.EBussiness.EntityFrameworkCore.Entity;

namespace Ly.EBussiness.Core.Dto.Order;

public class OrderListPageOutput(PageResult<OrderListDto> result)
    : PageResult<OrderListDto>(result.Items, result.TotalCount);

public class OrderListDto
{
    /// <summary> 订单号 </summary>
    public string OrderId { get; set; }

    /// <summary> 用户id </summary>
    public string UserId { get; set; }
    
    /// <summary> 订单状态 </summary>
    public OrderStatus OrderStatus { get; set; }

    /// <summary> 总金额 </summary>
    public decimal TotalAmount { get; set; }
}

public class OrderListInput : QueryPageBase
{
    public string UserId { get; set; }
    
    // /// <summary> 订单状态 </summary>
    // public OrderStatus OrderStatus { get; set; }

    public override string GetSelectSql()
    {
        return $"""
                 select *
                 {GetPageSelectTotalSql()}
                """;
    }

    protected override void ResolveCondition()
    {
        if (string.IsNullOrEmpty(UserId))
        {
            UserId = "1";
        }
        AddWhere(" and UserId = @UserId ",nameof(UserId),UserId);
    }

    protected override void ResolveSort()
    {
        AddSort(" creationTime ",SortType.Desc);
    }

    public override string GetPageSelectTotalSql()
    {
        return $"""
                from OrderInfo where 1=1
                """;
    }
}