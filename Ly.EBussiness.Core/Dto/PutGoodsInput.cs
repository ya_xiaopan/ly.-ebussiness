﻿using Ly.EBussiness.Common.DapperUtils.Page;
using Ly.EBussiness.EntityFrameworkCore.Entity.Enum;

namespace Ly.EBussiness.Core.Dto;

/// <summary>
/// 上架商品-查询
/// </summary>
public class PutGoodsInput : QueryPageBase
{
    /// <summary> 商品类型 </summary>
    public List<GoodsType>? GoodsTypes { get; set; }

    /// <summary> 商品大小 - level </summary>
    public List<GoodsLevel>? GoodsLevels { get; set; }

    /// <summary> 商品单位 </summary>
    public List<GoodsUnit>? GoodsUnits { get; set; }

    public override string GetSelectSql()
    {
        return @$" select 
     Id,
     GoodsType,
     GoodsLevel,
     GoodsUnit,
     MinPrice,
OriginalPrice,
Pic,
Name,
     Sum,
     CreationTime,
     DeletionTime
     {GetPageSelectTotalSql()} ";
    }
    
    // /// <summary> 单价 </summary>
    // public decimal MinPrice { get; set; }
    //
    // /// <summary> 单价 </summary>
    // public decimal OriginalPrice { get; set; }
    //
    // /// <summary> 图片 </summary>
    // public string Pic { get; set; }

    protected override void ResolveCondition()
    {
        if (GoodsTypes is not null && GoodsTypes.Count > 0)
            AddWhere(" and GoodsType in @GoodsTypes ", nameof(GoodsTypes), GoodsTypes);
        if (GoodsLevels is not null && GoodsLevels.Count > 0)
            AddWhere(" and GoodsLevel in @GoodsLevels ", nameof(GoodsLevels), GoodsLevels);
        if (GoodsUnits is not null && GoodsUnits.Count > 0)
            AddWhere(" and GoodsUnit in @GoodsUnits ", nameof(GoodsUnits), GoodsUnits);
    }

    protected override void ResolveSort()
    {
        AddSort(" CreationTime ", SortType.Desc);
    }

    public override string GetPageSelectTotalSql() => "  from PutGoods where 1=1 ";
}