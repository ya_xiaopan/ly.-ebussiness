﻿using Ly.EBussiness.EntityFrameworkCore.Entity.Enum;

namespace Ly.EBussiness.Core.Dto;

/// <summary>
/// 商品上架相关dto
/// </summary>
public class PutGoodsDto
{
    /// <summary> 商品类型 </summary>
    public GoodsType GoodsType { get; set; }

    /// <summary> 商品大小 - level </summary>
    public GoodsLevel GoodsLevel { get; set; }

    /// <summary> 商品单位 </summary>
    public GoodsUnit GoodsUnit { get; set; }

    /// <summary> 单价 </summary>
    public decimal Price { get; set; }

    /// <summary> 数量 - 暂时无用</summary>
    public int Sum { get; set; }

    public void Check()
    {
        if (Price <= 0) throw new Exception("单价必须大于0");
        // if (Sum <=0 0) throw new Exception("单价必须大于0");
    }
}