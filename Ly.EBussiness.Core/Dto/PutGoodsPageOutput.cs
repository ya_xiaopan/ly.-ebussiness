﻿using Ly.EBussiness.Common.DapperUtils.Page;
using Ly.EBussiness.EntityFrameworkCore.Entity;

namespace Ly.EBussiness.Core.Dto;

public class PutGoodsPageOutput(PageResult<PutGoods> result) : PageResult<PutGoods>(result.Items, result.TotalCount);