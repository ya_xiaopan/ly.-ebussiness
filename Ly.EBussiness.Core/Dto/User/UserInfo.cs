﻿using Ly.EBussiness.EntityFrameworkCore.Auth.User;
using Ly.EBussiness.EntityFrameworkCore.Entity;

namespace Ly.EBussiness.Core.Dto.User;

/// <summary>
/// 对外得用户信息
/// </summary>
public class UserInfoDto
{
    public string UserId { get; set; }

    public string UserName { get; set; }

    public RoleEnum Role { get; set; }

    public static UserInfoDto MapperUserInfoDto(UserInfo userInfo)
    {
        var userInfoDto = new UserInfoDto
        {
            UserId = userInfo.UserId,
            UserName = userInfo.UserName ?? "",
            Role = userInfo.Role
        };
        return userInfoDto;
    }
    
    
}