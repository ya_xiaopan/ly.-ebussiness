﻿using System.ComponentModel.DataAnnotations;

namespace Ly.EBussiness.Core.Dto.User;

public class UserLoginInput : IValidatableObject
{
    public string Code { get; set; }


    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        var result = new List<ValidationResult>();
        if (Code.IsNullOrEmpty())
        {
            result.Add(new ValidationResult("CODE不允许为空"));
        }
        return result;
    }
}