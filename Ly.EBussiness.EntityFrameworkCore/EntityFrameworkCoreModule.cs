﻿using Ly.EBussiness.EntityFrameworkCore.Auth.User;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Autofac;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Ly.EBussiness.EntityFrameworkCore
{
    [DependsOn(typeof(AbpEntityFrameworkCoreModule))]
    [DependsOn(typeof(AbpAutofacModule))]
    public class EntityFrameworkCoreModule : AbpModule
    {
        
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpDbContextOptions>(option =>
            {
                // 会自动注入连接自字符串 == 在 appsettings.json 文件中
                option.UseMySQL();
            });
            //添加依赖注入== 
            context.Services.AddAbpDbContext<EBussinessDbContext>(option =>
            {
                // 添加默认仓储
                option.AddDefaultRepositories();
                //所有的实体都添加仓储
                option.AddDefaultRepositories(includeAllEntities: true);
            });
            context.Services.AddSingleton<IReadOnlyQueryRepository, ReadOnlyQueryRepository>();
            context.Services.AddTransient<ICurrentUser, CurrentUser>();
        }

    }
}