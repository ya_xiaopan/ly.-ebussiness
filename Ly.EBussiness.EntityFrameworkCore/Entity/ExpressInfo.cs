﻿using System.ComponentModel.DataAnnotations.Schema;
using Ly.EBussiness.EntityFrameworkCore.BaseEntity;
using Volo.Abp.Auditing;

namespace Ly.EBussiness.EntityFrameworkCore.Entity;

/// <summary>
/// 
/// </summary>
[Table("ExpressInfo")]
[Audited]
public class ExpressInfo : BaseAuditEntity<int>
{
    /// <summary>
    /// 
    /// </summary>
    public string OrderId { get; set; }
    
    /// <summary>
    /// 收件省份
    /// </summary>
    public string RecipientProvince { get; set; }

    /// <summary>
    /// 收件城市
    /// </summary>
    public string RecipientCity { get; set; }

    /// <summary>
    /// 详细地址
    /// </summary>
    public string AcctExpress { get; set; }

    /// <summary>
    /// 收件人姓名
    /// </summary>
    public string RecipientName { get; set; }

    /// <summary>
    /// 收件人电话
    /// </summary>
    public string RecipientPhone { get; set; }

    /// <summary>
    /// 快递备注
    /// </summary>
    public string ExpressComment { get; set; }
}