﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Ly.EBussiness.EntityFrameworkCore.BaseEntity;
using Volo.Abp.Auditing;

namespace Ly.EBussiness.EntityFrameworkCore.Entity;

[Table("OrderInfo")]
[Audited]
public class OrderInfo : BaseAuditEntity<int>
{
    /// <summary> 订单号 </summary>
    public string OrderId { get; set; }

    /// <summary> 用户id </summary>
    public string UserId { get; set; }
    
    /// <summary> 订单状态 </summary>
    public OrderStatus OrderStatus { get; set; }
    
    /// <summary> 总金额 </summary>
    public decimal TotalAmount { get; set; }
    
    

    //todo 快递信息
}

/// <summary>
/// 订单状态
/// </summary>
public enum OrderStatus
{
    // 下单订单后 => 未付
    // 下单订单后超过 15 分 未支付 => 取消
    // 快递收到货后变为 => 已完成
    
    [Description("取消")]
    Cancel,
    [Description("未付")]
    UnPaid,
    [Description("已付")]
    Paid,
    [Description("已完成")]
    Completed,
}