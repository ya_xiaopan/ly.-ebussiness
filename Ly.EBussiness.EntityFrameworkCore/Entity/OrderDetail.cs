﻿using System.ComponentModel.DataAnnotations.Schema;
using Ly.EBussiness.EntityFrameworkCore.BaseEntity;
using Volo.Abp.Auditing;

namespace Ly.EBussiness.EntityFrameworkCore.Entity;

[Table("OrderDetail")]
[Audited]
public class OrderDetail : BaseAuditEntity<int>
{
    /// <summary> 订单号 </summary>
    public string OrderId { get; set; }
    
    /// <summary> 上架商品Id </summary>
    public int PutGoodsId { get; set; }
    
    /// <summary> 总金额 </summary>
    public decimal TotalPrice { get; set; }

    /// <summary> 数量-份数 </summary>
    public int Sum { get; set; }
    
    // 从上架的商品处读取
    // /// <summary> 单价 </summary>
    // public decimal Price { get; set; }
    // 
    // /// <summary> 商品类型 </summary>
    // public GoodsType GoodsType { get; set; }
    //
    // /// <summary> 商品大小 - level </summary>
    // public GoodsLevel GoodsLevel { get; set; }
    //
    // /// <summary> 商品单位 </summary>
    // public GoodsUnit GoodsUnit { get; set; }
}