﻿using System.ComponentModel;

namespace Ly.EBussiness.EntityFrameworkCore.Entity.Enum;

/// <summary>
/// 商品类型 红心 黄心
/// </summary>
public enum GoodsType
{
    [Description("红心")] Red,
    [Description("黄心")] Yellow,
}

// 小果 中果 大果
public enum GoodsLevel
{
    [Description("小果")] Small,
    [Description("中果")] Medium,
    [Description("大果")] Large
}

/// <summary>
/// 商品单位 斤 公斤 箱 g
/// </summary>
public enum GoodsUnit
{
    [Description("公斤")] Kg,
    [Description("斤")] Catty,
    [Description("箱")] Box,
    [Description("g")] g
}