﻿using System.ComponentModel.DataAnnotations.Schema;
using Ly.EBussiness.EntityFrameworkCore.BaseEntity;
using Ly.EBussiness.EntityFrameworkCore.Entity.Enum;
using Volo.Abp.Auditing;

namespace Ly.EBussiness.EntityFrameworkCore.Entity;

/// <summary>
/// 上架商品-上架后-不允许更改只允许更改数量  其他的想要更改必须先软删除在添加
/// 生效标记-没有被删除
/// 生效时间 创建时间-删除时间
/// </summary>
[Table("PutGoods")]
[Audited]
public class PutGoods : BaseAuditEntity<int>
{
    /// <summary> 商品类型 </summary>
    public GoodsType GoodsType { get; set; }
    
    /// <summary> 商品类型 </summary>
    public string Name { get; set; }

    /// <summary> 商品大小 - level </summary>
    public GoodsLevel GoodsLevel { get; set; }

    /// <summary> 商品单位 </summary>
    public GoodsUnit GoodsUnit { get; set; }

    /// <summary> 单价 </summary>
    public decimal MinPrice { get; set; }
    
    /// <summary> 单价 </summary>
    public decimal OriginalPrice { get; set; }
    
    /// <summary> 图片 </summary>
    public string Pic { get; set; }

    /// <summary> 数量 </summary>
    public int Sum { get; set; }

    // /// <summary> 是否有效 </summary>
    // public bool IsFlag { get; set; }
    //
    // /// <summary> 有效时间-开始时间 </summary>
    // public DateTime BeginTime { get; set; }
    //
    // /// <summary> 有效时间-结束时间 </summary>
    // public DateTime EndTime { get; set; }
}