﻿using System.ComponentModel.DataAnnotations.Schema;
using Ly.EBussiness.EntityFrameworkCore.Auth.User;
using Ly.EBussiness.EntityFrameworkCore.BaseEntity;
using Volo.Abp.Auditing;

namespace Ly.EBussiness.EntityFrameworkCore.Entity;

[Table("UserInfo")]
[Audited]
public class UserInfo : BaseAuditEntity<int>
{
    public string UserId { get; set; }
    public string? UserName { get; set; }

    public string? UserPassword { get; set; }
    public string? Iphone { get; set; }
    
    public RoleEnum Role{ get; set; }
}