﻿using Volo.Abp;

namespace Ly.EBussiness.EntityFrameworkCore.BaseEntity;

// public interface IFullAuditEntity:ISoftDelete
public interface IFullAuditEntity:IEBussinessSoftDelete
{
    
}

public interface IEBussinessSoftDelete
{
}