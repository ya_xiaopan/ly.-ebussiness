﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;

namespace Ly.EBussiness.EntityFrameworkCore.BaseEntity;

/// <summary>
/// 基础审计字段
/// </summary>
#nullable disable
public class BaseAuditEntity<TKey> : Entity<TKey>, IFullAuditEntity
{
    /// <summary>创建时间</summary>
    [Description("创建时间")]
    [Comment("创建时间")]
    [DisableAuditing]
    [Column("CreationTime")]
    public virtual DateTime CreationTime { get; set; }

    /// <summary>创建人</summary>
    [Description("创建人")]
    [Comment("创建人")]
    [DisableAuditing]
    [Column("CreatorId")]
    public virtual string CreatorId { get; set; }

    /// <summary>删除人</summary>
    [Description("删除人")]
    [Comment("删除人")]
    [DisableAuditing]
    [Column("DeleterId")]
    [CanBeNull]
    public virtual string DeleterId { get; set; }

    /// <summary>删除时间</summary>
    [Description("删除时间")]
    [Comment("删除时间")]
    [DisableAuditing]
    [Column("DeletionTime")]
    public virtual DateTime? DeletionTime { get; set; }

    /// <summary>是否删除</summary>
    [Description("是否删除")]
    [Comment("是否删除")]
    [Column("IsDeleted")]
    public virtual bool IsDeleted { get; set; }

    /// <summary>最后修改时间</summary>
    [Description("最后修改时间")]
    [Comment("最后修改时间")]
    [DisableAuditing]
    [Column("LastModificationTime")]
    public virtual DateTime? LastModificationTime { get; set; }

    /// <summary>最后修改人</summary>
    [Description("最后修改人")]
    [Comment("最后修改人")]
    [DisableAuditing]
    [Column("LastModifierId")]
    public virtual string LastModifierId { get; set; }

    /// <summary></summary>
    public virtual void Delete()
    {
        IsDeleted = true;
    }

    /// <summary></summary>
    public virtual void UnDelete()
    {
        IsDeleted = false;
    }

}