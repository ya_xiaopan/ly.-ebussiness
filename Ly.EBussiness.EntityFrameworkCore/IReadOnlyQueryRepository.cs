﻿using Ly.EBussiness.Common.DapperUtils.Page;

namespace Ly.EBussiness.EntityFrameworkCore;

/// <summary>
/// 
/// </summary>
public interface IReadOnlyQueryRepository
{
    /// <summary>
    /// 分页查询，允许通过选项决定查读库还是写库
    /// </summary>
    /// <typeparam name="TOutPut"></typeparam>
    /// <typeparam name="TInPut"></typeparam>
    /// <param name="queryPageNormal"></param>
    /// <returns></returns>
    Task<PageResult<TOutPut>> GetAllPagedAsync<TOutPut, TInPut>(TInPut queryPageNormal)
        where TInPut : QueryPageBase;

    /// <summary>
    /// 不分页查询，全量查询
    /// </summary>
    /// <typeparam name="TInPut"></typeparam>
    /// <typeparam name="TOutPut"></typeparam>
    /// <param name="queryBase"></param>
    /// <returns></returns>
    Task<IList<TOutPut>> GetAllAsync<TOutPut, TInPut>(TInPut queryBase)
        where TInPut : QueryBase;


    /// <summary>
    /// 查询只读节点，异步节点数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sql"></param>
    /// <param name="param"></param>
    /// <param name="dbConfig"></param>
    /// <returns></returns>
    Task<IList<T>> QueryAsync<T>(string sql, object param = null);

    /// <summary>
    /// 查询只读节点，异步节点数据
    /// 注意此处有sql参数总数不能超过2100个的限制，包含集合数量总参数不允许超过2100
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="param"></param>
    /// <param name="dbConfig"></param>
    /// <returns></returns>
    Task<IList<T>> QueryAsync<T>(object param);

    /// <summary>
    /// 查询数量
    /// </summary>
    /// <param name="param"></param>
    /// <param name="dbConfig"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    Task<int> CountAsync<T>(object param);

    /// <summary>查询单条记录</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sql"></param>
    /// <param name="param"></param>
    /// <param name="dbConfig"></param>
    /// <returns></returns>
    Task<T> FirstOrDefaultAsync<T>(string sql, object param = null);


    /// <summary>
    /// 查询单条记录-实体类
    /// </summary>
    /// <param name="param"></param>
    /// <param name="dbConfig"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    Task<T> FirstOrDefaultAsync<T>(object param = null)
        where T : class, new();
}