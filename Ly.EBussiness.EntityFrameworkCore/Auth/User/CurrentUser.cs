﻿using System.Security.Claims;
using Volo.Abp.Security.Claims;

namespace Ly.EBussiness.EntityFrameworkCore.Auth.User;

public sealed class CurrentUser : ICurrentUser
{
    private static readonly Claim[] EmptyClaimsArray = Array.Empty<Claim>();
    public string? Id => _principalAccessor.Principal?.FindUserId();

    public string? UserName => this.FindClaimValue(CustomClaimTypes.UserName);

    public string? Role => this.FindClaimValue(CustomClaimTypes.Role);

    private readonly ICurrentPrincipalAccessor _principalAccessor;
    /// <summary>
    /// 
    /// </summary>
    /// <param name="principalAccessor"></param>
    public CurrentUser(ICurrentPrincipalAccessor principalAccessor)
    {
        _principalAccessor = principalAccessor;
    }

    public Claim? FindClaim(string claimType)
    {
        return _principalAccessor.Principal?.Claims.FirstOrDefault(c => c.Type == claimType);
    }

    public Claim[] FindClaims(string claimType)
    {
        return _principalAccessor.Principal?.Claims.Where(c => c.Type == claimType).ToArray() ?? EmptyClaimsArray;
    }

    public Claim[] GetAllClaims()
    {
        return _principalAccessor.Principal?.Claims.ToArray() ?? EmptyClaimsArray;
    }
}