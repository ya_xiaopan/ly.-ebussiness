﻿namespace Ly.EBussiness.EntityFrameworkCore.Auth.User;

public class Role
{
    public const string CustomUser = nameof(CustomUser);
    public const string Admin = nameof(Admin);
}

public enum RoleEnum
{
    CustomUser,
    Admin
}