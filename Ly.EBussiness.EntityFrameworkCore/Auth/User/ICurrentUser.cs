﻿using System.Security.Claims;

namespace Ly.EBussiness.EntityFrameworkCore.Auth.User;

public interface ICurrentUser
{
    string? Id { get; }
    string? UserName { get; }

    string? Role { get; }

    Claim? FindClaim(string claimType);

    Claim[] FindClaims(string claimType);

    Claim[] GetAllClaims();
}