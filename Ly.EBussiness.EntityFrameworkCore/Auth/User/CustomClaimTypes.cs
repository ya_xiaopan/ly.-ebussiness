﻿namespace Ly.EBussiness.EntityFrameworkCore.Auth.User;

public static class CustomClaimTypes
{
    public const string UserId = "UserId";
    public const string UserName = "UserName";
    public const string Role = "Role";
}
