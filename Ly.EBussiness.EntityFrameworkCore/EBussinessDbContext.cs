﻿using Ly.EBussiness.EntityFrameworkCore.BaseEntity;
using Ly.EBussiness.EntityFrameworkCore.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;

namespace Ly.EBussiness.EntityFrameworkCore;

public partial class EBussinessDbContext : AbpDbContext<EBussinessDbContext>, IScopedDependency
{
    public DbSet<UserInfo> UserInfos { get; set; }
    
    public DbSet<OrderDetail> OrderDetails { get; set; }
    
    public DbSet<OrderInfo> OrderInfos { get; set; }
    
    public DbSet<PutGoods> PutGoods { get; set; }
    
    public DbSet<ExpressInfo> ExpressInfos { get; set; }
}