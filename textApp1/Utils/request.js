//请求封装
export default async function requestHttp(url, method, data) {
  //请求头设置
  var header = {
    Authorization: "Bearer " + wx.getStorageSync("login_token"),
    Accept: "application/json",
    "Content-Type": "application/json",
  }
  return new Promise((resolve, reject) => {
    wx.request({
      url: url,
      data: data,
      header: header,
      method: method,
      success: (res => {
        console.log(res);
        if (res.data.code === 200) {
          // wx.switchTab({ url: '/pages/index/index',});
          resolve(res)
        } else {
          if (res.statusCode === 401 || res.statusCode === 403 ) {
            wx.redirectTo({ url: '/pages/login/login',});
          }
          reject(res)
        }
      }),
      fail: (res => {
      })
    })
  })
}