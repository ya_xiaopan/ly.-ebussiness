import requestHttp from "../request"

export default async function getGoodsListPage() {
  console.log("确认这是一个函数:"+ requestHttp); // 确认这是一个函数
  var url="http://localhost:5263/Goods/QueryPutGoodsPage"
  var method="POST"
  var data={
    "maxResultCount": 0,
    "skipCount": 0,
    "goodsTypes": [
      0,1,2
    ],
    "goodsLevels": [
      0,1,2
    ],
    "goodsUnits": [
      0,1,2
    ]
  }
  return await requestHttp(url, method, data);
}

