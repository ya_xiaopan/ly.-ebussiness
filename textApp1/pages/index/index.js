import getGoodsListPage from "../../Utils/request/goods"
// 初始化动画实例
let animation = wx.createAnimation({
  duration: 400,
  timingFunction: "linear",
  delay: 0
});
Page({
  data: {
    goodsList: [],
    swiperList: [
      {navigator_url:"https://guli-0321.oss-cn-shenzhen.aliyuncs.com/2022/05/03/ed1a0276edbd4b6fb22983c8e2a16171%E9%A6%99%E8%95%89.png",
      picUrl:"https://guli-0321.oss-cn-shenzhen.aliyuncs.com/2022/05/03/ed1a0276edbd4b6fb22983c8e2a16171%E9%A6%99%E8%95%89.png"}
    ],
    catesList: ["1"],
    shoppingList:[],
    carTotalPrice: 0,
    carTotalSum: 0,
    movelength: 0,  // 上移或下拉动画的单位距离
    animationData: {} // 动画动作对象
  },
  //options(Object)
  onLoad: function (options) {
    this.getSwiperList();
    this.getCateList();
    this.getGoodsList();
  },
  getSwiperList() {
  },
  getCateList() {
  },
  getGoodsList() {
    console.log("getGoodsList");
    getGoodsListPage()
    .then(result => {
      console.log('商品列表页面数据:', result);
      this.setData({goodsList : result.data.data.items});
    })
    .catch(error => {
      console.error('获取商品列表页数据时出错:', error);
    });
  },
  handleToCategory(e) {
    getApp().globalData.custom = {
      index: e.currentTarget.dataset.index,
      id: e.currentTarget.dataset.id
    };

    wx.switchTab({
      url: '/pages/category/index',
      success: function () {
        var page = getCurrentPages().pop();
        if (page == undefined || page == null) return;
        page.onLoad();
      }
    });
  },

  // 点击‘+’添加进购物车
  addShopcart: function (e) {
    console.log("点击‘+’添加进购物车 :"+ e.target.dataset.id)
    let move_length = this.data.movelength;
    let shopping_list = this.data.shoppingList;
    let total_price = this.data.carTotalPrice;
    let total_count = this.data.carTotalSum + 1;
    total_price = parseInt(total_price) + parseInt(e.target.dataset.price);
    let itemNum = 1;
    let that = this;

    // 是否有同种商品判断
    if (this.data.shoppingList.length > 0) {
      // 商品名是否相同判断，不重复添加同名商品
      let isHave = this.data.shoppingList.findIndex(item => item.id == e.target.dataset.id)
      if (isHave != -1) {
        that.data.shoppingList[isHave].num++
      } else {
        // 购物车数组加进新的一样食品
        that.data.shoppingList.push({
          price: e.target.dataset.carTotalPrice,
          name: e.target.dataset.name,
          num: itemNum
        })
        // 动画效果的长度添加
        move_length++
      }
    // 没有商品时直接添加
    } else {
      this.data.shoppingList.push({
        price: e.target.dataset.carTotalPrice,
        name: e.target.dataset.name,
        num: itemNum
      })
      move_length++
    }
    // 动画上拉长度对应的bottom的计算
    /**
     *  mlength是bottom的长度
     *  animation.bottom(mlength).step() 加入动画队列
     */
    let mlength = move_length * 55;
    if (move_length > 1) {
      mlength = 55 + (move_length - 1) * 65;
    }
    this.animation = animation
    animation.bottom(mlength).step()
    this.setData({
      animationData: animation.export()
    })
    this.setData({
      shoppingList: shopping_list,
      carTotalPrice: total_price,
      carTotalSum: total_count,
      // 购物车当有商品时弹出
      cartIsHidden: false,
      movelength: move_length
    })
  },
});