﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace Ly.EBussiness.Common.DapperUtils;

/// <summary>
/// 
/// </summary>
public static class DapperHelp
{
    /// <summary>
    /// 获取实体类所有的属性
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static Dictionary<string, string> GetFields<T>()
    {
        var propertyInfos = ((IEnumerable<PropertyInfo>)typeof(T).GetProperties()).Where<PropertyInfo>((Func<PropertyInfo, bool>)(p => p.GetCustomAttribute<NotMappedAttribute>() == null));
        var fields = new Dictionary<string, string>();
        foreach (PropertyInfo propertyInfo in propertyInfos)
        {
            var prop = propertyInfo;
            if ((!prop.PropertyType.IsGenericType ||
                 !(prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))) &&
                prop.GetCustomAttribute<ForeignKeyAttribute>() == null)
            {
                ColumnAttribute customAttribute = prop.GetCustomAttribute<ColumnAttribute>();
                fields.Add(string.IsNullOrWhiteSpace(customAttribute?.Name) ? prop.Name : customAttribute.Name, prop.Name);
            }
        }
        return fields;
    }
}