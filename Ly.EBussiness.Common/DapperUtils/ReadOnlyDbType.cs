﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReadOnlyDbType.cs" company="">
//   
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.ComponentModel;

namespace Ly.EBussiness.Common.DapperUtils
{
    /// <summary> </summary>
    public enum ReadOnlyDbType
    {
        /// <summary> 企业自驾主库 同步只读节点 </summary>
        [Description("企业自驾主库 同步只读节点")]
        Apollo_GW_Entzj_EnterpriseCarRental_ReadOnly_TB,

        // /// <summary> 企业自驾主库 异步只读节点 </summary>
        // [Description(" 企业自驾主库 异步只读节点")]
        // Apollo_GW_Entzj_EnterpriseCarRental_ReadOnly_YB,

        /// <summary> 企业自驾主库 写库 </summary>
        [Description("企业自驾主库写库")]
        Apollo_GW_Entzj_EnterpriseCarRental,
        // /// <summary>
        // /// 代驾主库 异步只读节点
        // /// </summary>
        // [Description("代驾主库 异步只读节点")]
        // Apollo_DJ_EhaiDsgDriver_ReadOnly_YB,
    }
}