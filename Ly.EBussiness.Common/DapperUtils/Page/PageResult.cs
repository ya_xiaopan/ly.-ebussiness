﻿namespace Ly.EBussiness.Common.DapperUtils.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PageResult<T>
    {
        /// <summary>
        /// ctor
        /// </summary>
        public PageResult()
        {
        }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="items"></param>
        /// <param name="totalCount"></param>
        public PageResult(IList<T> items, int totalCount)
        {
            Items = items;
            TotalCount = totalCount;
        }

        /// <summary>
        ///  集合数据
        /// </summary>
        public IList<T> Items { get; set; }

        /// <summary>
        /// 总记录数
        /// </summary>
        public int TotalCount { get; set; }
    }
}