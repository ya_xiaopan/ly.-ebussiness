﻿namespace Ly.EBussiness.Common.DapperUtils.Page;

/// <summary>
/// 
/// </summary>
public class QueryWhereInfo
{
    /// <summary>
    ///  完整的 where 条件语句
    ///  例：o.OrderNo = @OrderNo
    /// </summary>
    public string WhereString { get; set; }
    /// <summary>
    /// sql 查询条件 替换名
    /// </summary>
    public string WhereParamName { get; set; }
    /// <summary>
    /// 查询条件值
    /// </summary>
    public object WhereParamValue { get; set; }
}