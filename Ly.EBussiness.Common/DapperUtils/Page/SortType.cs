﻿using System.ComponentModel;

namespace Ly.EBussiness.Common.DapperUtils.Page;

/// <summary>
/// 
/// </summary>
public enum SortType
{
    /// <summary>
    /// 
    /// </summary>
    [Description("升序")]
    Asc,
    /// <summary>
    /// 
    /// </summary>
    [Description("降序")]
    Desc
}