﻿// using Volo.Abp.Authorization.Permissions;
//
// namespace Ly.EBussiness.Auth;
//
// public class BookPermissionDefinitionProvider : PermissionDefinitionProvider
// {
//     public override void Define(IPermissionDefinitionContext context)
//     {
//         var myGroup = context.AddGroup("Auth");
//         var permission = myGroup.AddPermission(BasePermission.BookGroup);
//         permission.AddChild(BasePermission.BookAdd);
//         permission.AddChild(BasePermission.BookRemove);
//         permission.AddChild(BasePermission.BookUpdate);
//         permission.AddChild(BasePermission.BookSelect);
//     }
// }