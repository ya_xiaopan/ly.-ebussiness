﻿using Ly.EBussiness.EntityFrameworkCore.Auth.User;
using Microsoft.AspNetCore.Authorization;

namespace Ly.EBussiness.Auth;

public class AuthPolicyHandler: AuthorizationHandler<HasAllRolesRequirement>
{
    private readonly ICurrentUser _currentUser;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="currentUser"></param>
    public AuthPolicyHandler(ICurrentUser currentUser)
    {
        _currentUser = currentUser;
    }

    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HasAllRolesRequirement requirement)
    {
        if (requirement.Roles.Any(x=>x.Equals(_currentUser?.Role)))
        {
            context.Succeed(requirement);
        }
        return Task.CompletedTask;
    }
}

public class HasAllRolesRequirement : IAuthorizationRequirement
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="roles"></param>
    public HasAllRolesRequirement(params string[] roles)
    {
        Roles = roles;
    }
    public string[] Roles { get; }
}


public class PolicyName
{
    public const string DEFAULT = "default";
    public const string ADMIN = "amdin";
    public const string COMMON = "common";
}








 