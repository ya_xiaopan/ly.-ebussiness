﻿// using Ly.EBussiness.Core.Auth.User;
// using Microsoft.AspNetCore.Authorization;
// using Volo.Abp.Authorization;
//
// namespace Ly.EBussiness.Auth
// {
//     /// <summary>
//     /// 判断用户是否具有权限 -- 权限是否生效主要地方
//     /// </summary>
//     public class PermissionHandler : IAuthorizationHandler
//     {
//         private readonly ICurrentUser _currentUser;
//         /// <summary>
//         /// 
//         /// </summary>
//         /// <param name="currentUser"></param>
//         public PermissionHandler(ICurrentUser currentUser)
//         {
//             _currentUser = currentUser;
//         }
//         public Task HandleAsync(AuthorizationHandlerContext context)
//         {
//             var currentUserRole = _currentUser.Role;
//            
//             // 当前访问 Controller/Action 所需要的权限(策略授权)
//             IAuthorizationRequirement[] pendingRequirements = context.PendingRequirements.ToArray();
//             // 逐个检查
//             foreach (IAuthorizationRequirement requirement in pendingRequirements)
//             {
//                 if (currentUserRole == Role.Admin.ToString())
//                 {
//                     context.Succeed(requirement);
//                     return Task.CompletedTask;
//                 }
//                 
//                 var currentUserId = _currentUser.Id;
//                 context.Succeed(requirement);
//             }
//             return Task.CompletedTask;
//         }
//     }
// }