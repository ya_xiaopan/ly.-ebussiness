﻿using System.Linq.Dynamic.Core.Tokenizer;
using System.Security.Claims;
using Ly.EBussiness.EntityFrameworkCore;
using Ly.EBussiness.EntityFrameworkCore.Auth;
using Ly.EBussiness.EntityFrameworkCore.BaseEntity;
using Ly.EBussiness.EntityFrameworkCore.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Uow;

namespace Ly.EBussiness;

[Route("[controller]/[action]")]
[ApiController]
[UnitOfWork]
public class TestController : AbpController
{
    private readonly EBussinessDbContext _eBussinessDbContext;
    private readonly IUnitOfWorkManager _manager;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="eBussinessDbContext"></param>
    /// <param name="manager"></param>
    public TestController(EBussinessDbContext eBussinessDbContext, IUnitOfWorkManager manager)
    {
        _eBussinessDbContext = eBussinessDbContext;
        _manager = manager;
    }

    [HttpGet]
    public async Task<UserInfo> test1()
    {
        var user = new UserInfo()
        {
            UserName = "2",
            Iphone = Thread.CurrentThread?.ManagedThreadId.ToString(),
        };
        var entityEntry = await _eBussinessDbContext.UserInfos.AddAsync(user);
        var entityEntryEntity = entityEntry.Entity;
        var entry = _eBussinessDbContext.Entry(user);
        
        Console.WriteLine("1:" + entry.Property(x=>x.Id).CurrentValue);
        Console.WriteLine(entityEntryEntity.Id);
        user.UserName = "34";
        await _eBussinessDbContext.SaveChangesAsync();
        Console.WriteLine(entityEntryEntity.Id);
        return entityEntryEntity;
    }
    
    [HttpGet]
    public async Task<OrderInfo> testOrder()
    {
        var PutGoodsId = 11;
        var sum = 2;
        var firstOrDefault = _eBussinessDbContext.PutGoods.AsNoTracking().FirstOrDefault(x => x.Id == PutGoodsId);
        if (firstOrDefault is null)
        {
            return null;
        }

        await using var beginTransactionAsync = await _eBussinessDbContext.Database.BeginTransactionAsync();
        var firstOrDefaultAsync = await _eBussinessDbContext.UserInfos.FirstOrDefaultAsync() ?? new UserInfo();
        var orderInfo = new OrderInfo();
        orderInfo.UserId = firstOrDefaultAsync.UserId;
        orderInfo.OrderStatus = OrderStatus.UnPaid;
        orderInfo.TotalAmount = 0;
        await _eBussinessDbContext.OrderInfos.AddAsync(orderInfo);
        await _eBussinessDbContext.SaveChangesAsync();
        var orderDetail = new OrderDetail();
        orderDetail.PutGoodsId = firstOrDefault.Id;
        orderDetail.Sum = sum;
        orderDetail.TotalPrice = firstOrDefault.OriginalPrice * sum;
        orderDetail.OrderId = orderInfo.OrderId;
        await _eBussinessDbContext.OrderDetails.AddAsync(orderDetail);
        orderInfo.TotalAmount = orderDetail.TotalPrice;
        await _eBussinessDbContext.SaveChangesAsync();
        await beginTransactionAsync.CommitAsync();
        return orderInfo;
    }
    
    
    [HttpGet]
    public async Task<OrderInfo> testOrder2()
    {
        var firstOrDefaultAsync = await _eBussinessDbContext.UserInfos.FirstOrDefaultAsync() ?? new UserInfo();
        var orderInfo = new OrderInfo();
        orderInfo.UserId = firstOrDefaultAsync.UserId;
        orderInfo.OrderStatus = OrderStatus.UnPaid;
        orderInfo.TotalAmount = 0;
        orderInfo.IsDeleted = false;
        await _eBussinessDbContext.OrderInfos.AddAsync(orderInfo);
        await _eBussinessDbContext.SaveChangesAsync();
        return orderInfo;
    }
    // [HttpGet]
    // public string GetToken()
    // {
    //     // return new JwtHelp().getToken(null);
    // }
}