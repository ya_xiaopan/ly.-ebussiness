﻿using Asp.Versioning;
using Ly.EBussiness.Auth;
using Ly.EBussiness.Core.Dto;
using Ly.EBussiness.Core.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace Ly.EBussiness.Controller;

/// <summary>
/// 商品相关-控制器
/// </summary>
[Route("[controller]/[action]")]
[ApiController]
public class GoodsController: AbpController
{
    private readonly IGoodsService _goodsService;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="goodsService"></param>
    public GoodsController(IGoodsService goodsService)
    {
        _goodsService = goodsService;
    }

    /// <summary>
    /// 上架
    /// </summary>
    /// <param name="input"></param>
    [HttpPost]
    [Authorize(Policy = PolicyName.ADMIN)]
    public async Task PutGoodsAsync(PutGoodsDto input)
    {
        await _goodsService.PutGoodsAsync(input);
    }
    
    [HttpPost]
    [Authorize(Policy = PolicyName.COMMON)]
    public async Task<PutGoodsPageOutput> QueryPutGoodsPageAsync(PutGoodsInput input)
    {
        return await _goodsService.QueryPutGoodsAsync(input);
    }
    
    /// <summary>
    /// 下架商品
    /// </summary>
    /// <param name="id"></param>
    [HttpGet]
    [Authorize(Policy = PolicyName.ADMIN)]
    public async Task PutDownGoodsAsync(int id)
    {
        await _goodsService.PutDownGoodsAsync(id);
    }
}