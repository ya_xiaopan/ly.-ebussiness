﻿using Ly.EBussiness.Auth;
using Ly.EBussiness.Core.Dto.Order;
using Ly.EBussiness.Core.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace Ly.EBussiness.Controller;

/// <summary>
/// 商品相关-控制器
/// </summary>
[Route("[controller]/[action]")]
[ApiController]
public class OrderController : AbpController
{
    private readonly IOrderService _orderService;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="orderService"></param>
    public OrderController(IOrderService orderService)
    {
        _orderService = orderService;
    }

    /// <summary>
    /// 查询自己的订单列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [Authorize(Policy = PolicyName.DEFAULT)]
    public async Task<OrderListPageOutput> QueryOrdersInfoPageAsync(OrderListInput input)
    {
        return await _orderService.QueryOrdersInfoPageAsync(input);
    }

    /// <summary>
    /// 查询自己的订单详情
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [Authorize(Policy = PolicyName.DEFAULT)]
    public async Task<OrderDetailsDto> QueryOrderDetailAsync(OrderDetailsInput input)
    {
        return await _orderService.QueryOrderDetailAsync(input);
    }
    
    
    /// <summary>
    /// 下单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [Authorize(Policy = PolicyName.DEFAULT)]
    public async Task<bool> OrderAsync(OrderDto input)
    {
        return await _orderService.OrderAsync(input);
    }
    
}