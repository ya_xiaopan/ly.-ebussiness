﻿using Ly.EBussiness.Core.Dto.User;
using Ly.EBussiness.Core.Manager.Dto.LoginDto;
using Ly.EBussiness.Core.Service;
using Ly.EBussiness.Filter.WrapResult;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace Ly.EBussiness.Controller;
/// <summary>
/// 商品相关-控制器
/// </summary>
[Route("[controller]/[action]")]
[ApiController]
public class UserController: AbpController
{
    private readonly IUserService _userService;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userService"></param>
    public UserController(IUserService userService)
    {
        _userService = userService;
    }

    [HttpPost]
    // [Authorize(Roles = Role.CustomUser)]
    public async Task<LoginResult> UserLoginAsync(UserLoginInput input)
    {
        return await _userService.UserLoginAsync(input);
    }
}