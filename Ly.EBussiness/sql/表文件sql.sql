CREATE TABLE `userinfo`
(
    `id`                   int                                                           NOT NULL AUTO_INCREMENT COMMENT '主键',
    `userId`               varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL,
    `userName`             int                                                           DEFAULT NULL COMMENT '用户昵称',
    `userPassword`         int                                                           DEFAULT NULL COMMENT '密码',
    `iphone`               varchar(20) COLLATE utf8mb4_general_ci                        DEFAULT NULL COMMENT '用户电话',
    `CreationTime`         datetime                                                      NOT NULL,
    `CreatorId`            varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `DeleterId`            varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
    `LastModifierId`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `DeletionTime`         datetime                                                      DEFAULT NULL,
    `LastModificationTime` datetime                                                      NOT NULL,
    `IsDeleted`            bit(1)                                                        DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 11
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='用户信息表';


CREATE TABLE `PutGoods`
(
    `id`                   int                                                           NOT NULL AUTO_INCREMENT COMMENT '主键',
    `GoodsType`            bit(10)                                                       NOT NULL COMMENT '商品类型',
    `GoodsLevel`           bit(10)                                                       NOT NULL COMMENT '商品大小',
    `GoodsUnit`            bit(10)                                                       NOT NULL COMMENT '商品单位',
    `Sum`                  int                                                           DEFAULT NULL COMMENT '数量',
    `Price`                decimal                                                       NOT NULL COMMENT '单价',

    `CreationTime`         datetime                                                      NOT NULL,
    `CreatorId`            varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `DeleterId`            varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
    `LastModifierId`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `DeletionTime`         datetime                                                      DEFAULT NULL,
    `LastModificationTime` datetime                                                      NOT NULL,
    `IsDeleted`            bit(1)                                                        DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 11
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='上架商品';


CREATE TABLE `OrderDetail`
(
    `id`                   int                                                           NOT NULL AUTO_INCREMENT COMMENT '主键',
    `OrderId`              varchar(20) COLLATE utf8mb4_general_ci                        NOT NULL COMMENT '订单号',
    `PutGoodsId`           int                                                           NOT NULL COMMENT '上架商品id',

    `TotalPrice`           decimal                                                       NOT NULL COMMENT '总金额',
    `Sum`                  int                                                           NOT NULL COMMENT '数量-份数',


    `CreationTime`         datetime                                                      NOT NULL,
    `CreatorId`            varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `DeleterId`            varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
    `LastModifierId`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `DeletionTime`         datetime                                                      DEFAULT NULL,
    `LastModificationTime` datetime                                                      NOT NULL,
    `IsDeleted`            bit(1)                                                        DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 11
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='订单详情';


CREATE TABLE `OrderInfo`
(
    `id`                   int                                                           NOT NULL AUTO_INCREMENT COMMENT '主键',
    `OrderId`              varchar(20) COLLATE utf8mb4_general_ci                        NOT NULL COMMENT '订单号',
    `UserId`               varchar(20) COLLATE utf8mb4_general_ci                        NOT NULL COMMENT '用户id',
    `TotalAmount`          decimal                                                       NOT NULL COMMENT '总金额',

    `CreationTime`         datetime                                                      NOT NULL,
    `CreatorId`            varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `DeleterId`            varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
    `LastModifierId`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `DeletionTime`         datetime                                                      DEFAULT NULL,
    `LastModificationTime` datetime                                                      NOT NULL,
    `IsDeleted`            bit(1)                                                        DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 11
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='订单详情';



CREATE TABLE `ExpressInfo`
(
    `id`                   int                                                           NOT NULL AUTO_INCREMENT COMMENT '主键',
    `OrderId`              varchar(20) COLLATE utf8mb4_general_ci                        NOT NULL COMMENT '订单号',
    `RecipientProvince`    varchar(100) COLLATE utf8mb4_general_ci                       NOT NULL COMMENT '收件省份',
    `RecipientCity`        varchar(100) COLLATE utf8mb4_general_ci                       NOT NULL COMMENT '收件城市',
    `AcctExpress`          varchar(100) COLLATE utf8mb4_general_ci                       NOT NULL COMMENT '详细地址',
    `RecipientName`        varchar(100) COLLATE utf8mb4_general_ci                       NOT NULL COMMENT '收件人姓名',
    `RecipientPhone`       varchar(100) COLLATE utf8mb4_general_ci                       NOT NULL COMMENT '收件人电话',
    `ExpressComment`       varchar(2000) COLLATE utf8mb4_general_ci                      NOT NULL COMMENT '快递备注',

    `CreationTime`         datetime                                                      NOT NULL,
    `CreatorId`            varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `DeleterId`            varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
    `LastModifierId`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `DeletionTime`         datetime                                                      DEFAULT NULL,
    `LastModificationTime` datetime                                                      NOT NULL,
    `IsDeleted`            bit(1)                                                        DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 11
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='快递信息表';


