﻿namespace Ly.EBussiness.Filter.WrapResult;
public class ApiResult
{
    public bool IsSuccess { get; set; }

    public string Message { get; set; }

    public int Code { get; set; }

    public string OperationId { get; set; }


    public static ApiResult Sucess(string operationId)
    {
        return new ApiResult()
        {
            IsSuccess = true,
            OperationId = operationId
        };
    }

    public static ApiResult<T> Sucess<T>(T result, string operationId)
    {
        return new ApiResult<T>()
        {
            IsSuccess = true,
            OperationId = operationId,
            Data = result
        };
    }

    public static ApiResult Error(int errorCode, string message, string operationId)
    {
        return new ApiResult()
        {
            Code = errorCode,
            Message = message,
            IsSuccess = false,
            OperationId = operationId
        };
    }
}

/// <summary>
///     Library内部的ApiResult
/// </summary>
/// <typeparam name="T">返回的数据类型</typeparam>
public class ApiResult<T> : ApiResult
{
    public T Data { get; set; }
}