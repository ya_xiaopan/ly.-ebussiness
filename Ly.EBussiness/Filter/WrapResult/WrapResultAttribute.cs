﻿// using Microsoft.AspNetCore.Mvc;
// using Microsoft.AspNetCore.Mvc.Filters;
//
// namespace Ly.EBussiness.Filter.WrapResult;
//
// /// <summary>
// /// 
// /// </summary>
// [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
// public class WrapResultAttribute : ResultFilterAttribute
// {
//     public override void OnResultExecuted(ResultExecutedContext context)
//     {
//         if (context.Result is ObjectResult { Value: not null } result)
//         {
//             var data = result.Value;
//             var response = new ApiResult<object>
//             {
//                 Code = 200, // 或者根据实际情况设置
//                 Message = "Success",
//                 Result = data
//             };
//             result.DeclaredType = typeof(ApiResult<object>);
//             result.Value = response;
//             // context.HttpContext.Response.Body = 
//         }
//         base.OnResultExecuted(context);
//     }
// }