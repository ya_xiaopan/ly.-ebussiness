﻿using System.Net;
using Ly.EBussiness.Filter.WrapResult;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

/// <summary>
/// 
/// </summary>
public class ResultFilter : IResultFilter

{
    public void OnResultExecuted(ResultExecutedContext context)
    {
        //返回结果之后
    }

    public void OnResultExecuting(ResultExecutingContext context)
    {
        if (context.Result is ObjectResult { Value: not null } result)
        {
            var data = result.Value;
            var response = new ApiResult<object>
            {
                Code = 200, // 或者根据实际情况设置
                Message = "Success",
                Data = data
            };
            result.DeclaredType = typeof(ApiResult<object>);
            result.Value = response;
            // 返回状态码设置为200，表示成功
            result.StatusCode = (int)HttpStatusCode.OK;
            // 设置返回格式
        };
    }
}