﻿
using Ly.EBussiness;

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseAutofac();  //Add this line
builder.Services.AddSwaggerGen();
builder.Services.ReplaceConfiguration(builder.Configuration);
builder.Services.AddApplication<EBussinessModule>();
var app = builder.Build();
app.InitializeApplication();
app.Run();